# Jenkins Assignment 6

**Create a CI pipeline to build the maven application along with the unit test, scans for code quality, vulnerability, code coverage**
* Integrate your Jenkins instance with Artifactory. 
* Publish the final artifacts to artifactory.

Solution: Pipeline Used
```bash
pipeline {
    agent any
  tools {
        maven 'maven'
        jdk 'java'
    }
    stages {
       
                
        stage('SCM Checkout') {
            steps {
                // Get some code from a GitHub repository
                checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[url: 'https://github.com/deegupta123/spring3hibernate.git']]])
                echo "Checkout cmpleted"
            }
        }
         stage('Build') {
            steps {
                sh 'mvn -Dmaven.test.failure.ignore=true install'
                echo 'Build completed'
            }
        }
        stage('Checkstyle') {
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
        }
        stage ('PMD') {
            steps {
                sh 'mvn pmd:pmd'
            }
        }
        stage ('Cobertura-Annalysis') {
            steps {
                sh 'mvn cobertura:cobertura'
            }
        }
        
        stage('Artifact Upload'){
            steps {
              nexusArtifactUploader artifacts: [[artifactId: 'Spring3HibernateApp', 
              classifier: '', file: 'target/Spring3HibernateApp.war', type: 'war']], 
              credentialsId: 'nexus', groupId: 'Spring3HibernateApp', 
              nexusUrl: '3.6.39.146:8081', nexusVersion: 'nexus3', 
              protocol: 'http', repository: 'springHibernate', 
              version: '1.8-SNAPSHOT'
            }
        }
      
        
        
            stage('Email') {
                steps {
                    emailext body: 'Hi The job is successfull', subject: 'Job Success', to: 'deepak.gupta@mygurukulam.org'
                }
            }
            }
        
    
}
```
* For Atifactory have setup Nexus and integrated it with Jenkins, need to install plugin Artifact Nexus Uploader , many ways to achieve by **pipeline** or by **maven project** have followed both above given is the script and below using Maven Project for this, for following this you need to update the artifactory URL in pom.xml file by giving the URL of **nexus repository** and have to add the nexus credentials settings in .m2/settings.xml of .m2 folder of Jenkins home directory or can create **nexus credentials in Jenkins** itself.

Goals Achieved

```bash
clean install deploy
```
Below SnapShots are attached :

Nexus Setup 

![](jenkins6/images/1.PNG)

Repository Creation in Nexus

![](jenkins6/images/2.PNG)

Pipeline Syntax Creation for Artifact Upload

![](jenkins6/images/3.PNG)

O/P of Artifacts Upload

![](jenkins6/images/7.PNG)

Nexus Artifact Upload

![8](/uploads/f7caad2bceac284587aa777647fbbb79/8.PNG)



